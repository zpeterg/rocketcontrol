# Rocket Control
A control panel for a Rocket

# Raspberry PI screen settings
Edit `/boot/config.txt` to have content:
```text
disable_overscan=0
overscan_left=0
overscan_right=120
overscan_top=0
overscan_bottom=150
hdmi_group=2
hdmi_mode=16
```