'use string'

const bresenham = require('bresenham')
const Canvas = require('drawille')
const ioHook = require('iohook')

const columns = process.stdout.columns * 2
const rows = process.stdout.rows * 4
const keyMap = { 37: [-3, 0], 38: [0, -3], 39: [3, 0], 40: [0, 3] }
const middleX = Math.round(columns / 2)
const middleY = Math.round(rows / 2)
const buttonHeight = 10
const buttonWidth = 20

console.log('columns/rows', columns, rows)
let x = middleX
let y = middleY
let downTimeout = null
let drawInterval = null

const c = new Canvas()

function drawButton(c) {
  const line = [
    ...bresenham(middleX - buttonHeight / 2, middleY + buttonWidth / 2, middleX + buttonWidth, middleY + buttonHeight),
    ...bresenham(middleX - buttonHeight / 2, middleY - buttonWidth / 2, middleX + buttonWidth, middleY - buttonHeight),
  ]
  line.forEach((point) => {
    c.set(point.x, point.y)
  })
}

function draw() {
  c.clear()
  drawButton(c)
  //console.log('hi')
  c.toggle(x-1, y)
  c.toggle(x+1, y)
  c.toggle(x, y-1)
  c.toggle(x, y+1)
  c.toggle(x, y)
  //line(20, 20, 50, 50, c.set.bind(c))
  process.stdout.write(c.frame())
}

function getDirection({ rawcode: keyCode }) {
  if (keyMap[keyCode]) {
    return keyMap[keyCode]
  }
}

ioHook.on('keydown', (e) => {
  // Move once
  const direction = getDirection(e)
  if (direction) {
    x += direction[0]
    y += direction[1]
    // Limit to sides
    if (x < 1) x = 1
    if (x > columns - 4) x = columns - 4
    if (y < 4) y = 4
    if (y > rows - 1) y = rows - 1
    draw()
  }
})

ioHook.start()
